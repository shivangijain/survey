import React from 'react';
import ReactDOM from 'react-dom';
import SurveyJson from './SurveyJson';

ReactDOM.render(
  <SurveyJson />,
  document.getElementById('surveyElement')
);