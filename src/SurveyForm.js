import React, { Component } from 'react';
import * as Survey from 'survey-react';
Survey.Survey.cssType = 'bootstrap';
Survey.defaultBootstrapCss.navigationButton = 'btn btn-green';

class SurveyForm extends Component {
  state = {
    abc: {
      name: 'page1',
      questions: [
        {
          type: 'text',
          name: 'age',
          title: 'enter age'
        }
      ]
    }
  };
  sendDataToServer(survey) {
    var resultAsString = JSON.stringify(survey.data);
    alert(resultAsString); //send Ajax request to your web server.
  }

  render() {
    const a = JSON.stringify(this.props.data);
    // const a = this.props.data

    // console.log(typeof JSON.parse(a))
    // const a =
    var obj = JSON.parse(a);
    // console.log(obj)

    console.log('Mo', this.props.data);

    return (
      <div>
        <Survey.Survey
          json={this.props.data}
          onComplete={this.sendDataToServer}
        />
      </div>
    );
  }
}

export default SurveyForm;
