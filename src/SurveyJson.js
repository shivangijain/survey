import React, { Component } from 'react';
import SurveyForm from './SurveyForm';

class SurveyJson extends Component {
  constructor(props) {
    super(props);
    this.state = { data: {}, openForm: false };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const value = event.target.value.replace(/\s/g, '');
    this.setState({ data: JSON.parse(value) });
  }

  handleSubmit(event) {
    console.log(event);
    //this.setState({ data: JSON.parse(event.target.value) });
    this.setState({ openForm: true });
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Enter Your JSON data:
            <input type="area" name="name" onChange={this.handleChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
        {this.state.openForm && <SurveyForm data={this.state.data} />}
      </div>
    );
  }
}

export default SurveyJson;
